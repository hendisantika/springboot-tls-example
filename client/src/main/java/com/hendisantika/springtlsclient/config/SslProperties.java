package com.hendisantika.springtlsclient.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/05/21
 * Time: 10.21
 */
@ConfigurationProperties("server.ssl")
@Getter
@Setter
public class SslProperties {

    private Resource trustStore;

    private String trustStorePassword;

    private String trustStoreType;

    private Resource keyStore;

    private String keyStorePassword;

    private String keyPassword;

    private String keyStoreType;
}