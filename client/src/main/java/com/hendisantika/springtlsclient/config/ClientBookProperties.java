package com.hendisantika.springtlsclient.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/05/21
 * Time: 10.16
 */
@ConfigurationProperties("client.book")
@Getter
@Setter
public class ClientBookProperties {

    private String scheme;

    private String host;

    private int port = -1;

    private int connectionTimeout = 1000;

    private int readTimeout = 1000;

    private String username;

    private String password;
}