package com.hendisantika.springtlsclient.exception;

import com.hendisantika.springtlsclient.model.ErrorInfo;
import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 11.05
 */
public class DataNotFoundException extends RuntimeException {

    @Getter
    private final transient ErrorInfo errorInfo;

    public DataNotFoundException(ErrorInfo errorInfo) {
        this.errorInfo = errorInfo;
    }
}