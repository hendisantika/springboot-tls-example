package com.hendisantika.springtlsclient.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.springtlsclient.model.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 11.07
 */
public class CustomErrorHandler extends DefaultResponseErrorHandler {

    private final ObjectMapper objectMapper;

    public CustomErrorHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    protected void handleError(ClientHttpResponse response,
                               HttpStatus statusCode) throws IOException {
        if (statusCode == HttpStatus.NOT_FOUND) {
            String message = new String(getResponseBody(response));
            ErrorInfo info = objectMapper.readValue(message, ErrorInfo.class);
            throw new DataNotFoundException(info);
        }
    }
}
