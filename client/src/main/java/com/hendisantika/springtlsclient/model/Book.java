package com.hendisantika.springtlsclient.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 10.39
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Book extends BookRequest {

    private UUID id;
}