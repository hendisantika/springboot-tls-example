package com.hendisantika.springtlsclient.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 11.06
 */
@NoArgsConstructor
@Getter
@Setter
public class ErrorInfo implements Serializable {

    private int code;

    private String type;

    private String message;
}