package com.hendisantika.springtlsclient.service;

import com.hendisantika.springtlsclient.model.Book;
import com.hendisantika.springtlsclient.model.BookRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 10.39
 */
@Service
public class BookService {

    private final RestTemplate restTemplate;

    @Autowired
    public BookService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Book create(BookRequest request) {
        return restTemplate.postForObject("/books", request, Book.class);
    }

    public Book read(UUID id) {
        return restTemplate.getForObject("/books/{0}", Book.class, id);
    }

}
