package com.hendisantika.springtlsserver.controller;

import com.hendisantika.springtlsserver.exception.ErrorInfo;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/21
 * Time: 20.09
 */
class CustomErrorControllerTest {
    @Mock
    private ErrorAttributes errorAttributes;

    @InjectMocks
    private CustomErrorController controller;

    private MockHttpServletRequest request;

    private MockHttpServletResponse response;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(
                new ServletRequestAttributes(request));

        response = new MockHttpServletResponse();
    }

    @Test
    public void testError() {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("message", "test-message");
        attributes.put("error", "test-error");

        when(errorAttributes.getErrorAttributes(any(WebRequest.class),
                anyBoolean())).thenReturn(attributes);

        ResponseEntity<ErrorInfo>
                entity = controller.error(request, response);
        assertNotNull(entity);
        ErrorInfo errorInfo = entity.getBody();
        assertNotNull(errorInfo);
        assertEquals("test-message", errorInfo.getMessage());
        assertNotNull(controller.getErrorPath());
    }

    @Test
    public void testErrorWithException() {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("message", "test-message");
        attributes.put("error", "test-error");

        when(errorAttributes.getErrorAttributes(any(WebRequest.class),
                anyBoolean())).thenReturn(attributes);

        RequestRejectedException exp =
                new RequestRejectedException("Just a test.");
        when(errorAttributes.getError(any(WebRequest.class))).thenReturn(exp);

        ResponseEntity<ErrorInfo>
                entity = controller.error(request, response);
        assertNotNull(entity);
        ErrorInfo errorInfo = entity.getBody();
        assertNotNull(errorInfo);
        assertEquals("test-message", errorInfo.getMessage());
        assertNotNull(controller.getErrorPath());
        assertEquals(400, errorInfo.getCode());
    }
}