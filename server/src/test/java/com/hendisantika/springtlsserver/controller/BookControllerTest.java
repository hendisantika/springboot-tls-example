package com.hendisantika.springtlsserver.controller;

import com.hendisantika.springtlsserver.dto.BookRequest;
import com.hendisantika.springtlsserver.entity.Book;
import com.hendisantika.springtlsserver.exception.DataNotFoundException;
import com.hendisantika.springtlsserver.service.BookService;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/05/21
 * Time: 05.35
 */
class BookControllerTest {
    @Mock
    private BookService service;

    @InjectMocks
    private BookController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() {
        Book book = new Book();
        book.setId(UUID.randomUUID());
        book.setTitle("Indra's Chronicle");
        book.setAuthor("Indra");
        when(service.create(any(BookRequest.class))).thenReturn(book);

        BookRequest request = new BookRequest("Indra's Chronicle", "Indra");
        Book result = controller.create(request);
        assertNotNull(result);
        assertEquals(book, result);
    }

    @Test
    public void testRead() {
        Book book = new Book();
        book.setId(UUID.randomUUID());
        book.setTitle("Indra's Chronicle");
        book.setAuthor("Indra");
        when(service.read(any(UUID.class))).thenReturn(book);

        Book result = controller.read(UUID.randomUUID());
        assertNotNull(result);
        assertEquals(book, result);
    }

    @Test
    public void testDataNotFoundRead() {
        Assertions.assertThrows(DataNotFoundException.class, () -> {
            when(service.read(any(UUID.class))).thenThrow(
                    new DataNotFoundException("Not Found!"));
            controller.read(UUID.randomUUID());
        });
    }

}