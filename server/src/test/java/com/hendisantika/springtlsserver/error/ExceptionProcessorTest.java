package com.hendisantika.springtlsserver.error;

import com.hendisantika.springtlsserver.exception.DataNotFoundException;
import com.hendisantika.springtlsserver.exception.ErrorInfo;
import com.hendisantika.springtlsserver.exception.ExceptionProcessor;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/05/21
 * Time: 07.20
 */
public class ExceptionProcessorTest {

    private ExceptionProcessor processor;
    private MockHttpServletRequest request;
    private String message;
    private String path;

    @Before
    public void setUp() throws Exception {
        processor = new ExceptionProcessor();
        request = new MockHttpServletRequest();
        message = "some message";
        path = "/some/path";
        request.setRequestURI(path);
        request.setPathInfo(path);
    }

    @Test
    public void testHandleDataNotFoundException() throws Exception {
        DataNotFoundException exception = new DataNotFoundException(message);
        ErrorInfo info = processor.handleDataNotFoundException(request, exception);
        validate(info, HttpStatus.NOT_FOUND, message);
    }

    private void validate(ErrorInfo info, HttpStatus status, String msg) {
        assertEquals(msg, info.getMessage());
        assertEquals(status.value(), info.getCode());
        assertEquals(status.getReasonPhrase(), info.getType());
        assertEquals(path, info.getPath());
    }
}
