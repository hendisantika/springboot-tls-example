package com.hendisantika.springtlsserver;

import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/05/21
 * Time: 07.19
 */
public class GenerateEncodedPassword {

    public static void main(String... args) {
        PasswordEncoder passwordEncoder =
                PasswordEncoderFactories.createDelegatingPasswordEncoder();

        String encodedPwd = passwordEncoder.encode("passwordA");
        System.out.println("passwordA: " + encodedPwd);

        encodedPwd = passwordEncoder.encode("passwordB");
        System.out.println("passwordB: " + encodedPwd);

    }
}
