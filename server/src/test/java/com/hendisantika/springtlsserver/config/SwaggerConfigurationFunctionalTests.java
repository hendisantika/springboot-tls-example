package com.hendisantika.springtlsserver.config;

import com.hendisantika.springtlsserver.ServerApplication;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/21
 * Time: 20.05
 */
@SpringBootTest(classes = {ServerApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class SwaggerConfigurationFunctionalTests {

    @Autowired
    ApplicationContext context;
    @Value("${local.server.port}")
    private Integer port;
    @Value("${server.ssl.key-store}")
    private String pathToJks;
    @Value("${server.ssl.key-store-password}")
    private String password;

    @Before
    public void startUp() {
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.config().getSSLConfig()
                .with().keyStore(pathToJks, password);
    }

    @Test
    public void testApi() {
        Response response = given()
                .contentType(ContentType.JSON)
                .baseUri("https://localhost")
                .port(port)
                .contentType(ContentType.JSON)
                .get("/v2/api-docs?group=book");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertNotNull(response.getBody().prettyPrint());
    }
}
