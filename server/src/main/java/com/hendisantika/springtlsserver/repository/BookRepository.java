package com.hendisantika.springtlsserver.repository;

import com.hendisantika.springtlsserver.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/21
 * Time: 15.24
 */
@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {
}
