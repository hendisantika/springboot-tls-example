package com.hendisantika.springtlsserver.config;

import com.hendisantika.springtlsserver.entity.Endpoint;
import com.hendisantika.springtlsserver.entity.User;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/21
 * Time: 16.48
 */
@ConfigurationProperties("security.auth")
@Data
public class SecurityAuthProperties {

    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    private static final String ROLE_PREFIX = "ROLE_";
    private Map<String, Endpoint> endpoints = new HashMap<>();

    private Map<String, User> users = new HashMap<>();

    @PostConstruct
    public void init() {
        endpoints.forEach((key, value) -> {
            List<String> roles = new ArrayList<>();
            for (String role : value.getRoles()) {
                roles.add(ROLE_PREFIX + role);
            }
            value.setRoles(roles.toArray(new String[0]));
        });

        users.forEach((key, value) -> {
            if (value.getId() == null) {
                value.setId(key);
            }

            if (value.getEncoding() != null) {
                value.setPassword(
                        "{" + value.getEncoding().trim() + "}"
                                + value.getPassword());
            } else {
                value.setPassword("{noop}" + value.getPassword());
            }
        });
    }

}
