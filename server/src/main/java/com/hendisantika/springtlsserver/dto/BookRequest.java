package com.hendisantika.springtlsserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/21
 * Time: 16.42
 */
@Getter
public class BookRequest {

    private final String title;

    private final String author;

    @JsonCreator
    public BookRequest(@JsonProperty("title") String title,
                       @JsonProperty("author") String author) {
        this.title = title;
        this.author = author;
    }
}