package com.hendisantika.springtlsserver.entity;

import lombok.Data;
import org.springframework.http.HttpMethod;

import static com.hendisantika.springtlsserver.config.SecurityAuthProperties.ROLE_ANONYMOUS;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/21
 * Time: 16.51
 */
@Data
public class Endpoint {

    private String path;

    private HttpMethod[] methods;

    private String[] roles;

    public String[] getRoles() {
        if (roles == null || roles.length == 0) {
            roles = new String[1];
            roles[0] = ROLE_ANONYMOUS;
        }

        return roles;
    }
}
