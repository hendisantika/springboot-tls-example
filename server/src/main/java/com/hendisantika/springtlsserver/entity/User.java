package com.hendisantika.springtlsserver.entity;

import lombok.Data;

import static com.hendisantika.springtlsserver.config.SecurityAuthProperties.ROLE_ANONYMOUS;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-tls-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/21
 * Time: 16.50
 */
@Data
public class User {
    private String id;

    private String encoding;

    private String password;

    private String[] roles;

    public String[] getRoles() {
        if (roles == null || roles.length == 0) {
            roles = new String[1];
            roles[0] = ROLE_ANONYMOUS;
        }

        return roles;
    }
}
